provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "example" {
  ami = "ami-09a5b0b7edf08843d"
  instance_type = "t2.micro"
  user_data = <<-EOF
              #!/bin/bash
              yum install httpd -y
              service httpd start
              echo 'hello world' > /var/www/html/index.html
              EOF
  key_name = "mykeypair"
  tags = {
    Name = "terraform-sample"
  }
}